import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailComponent } from './mail.component';

describe('MailComponent', () => {
  let component: MailComponent;
  let fixture: ComponentFixture<MailComponent>;
  const myComponent = new MailComponent();

  myComponent.Name = 'Mario';
  myComponent.Surname = 'Rossi';
  myComponent.Number = '1';
  myComponent.Domain = 'gmail.com';

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain a @', () => {
    myComponent.genMail();
    expect(myComponent.Mail).toContain('@');
  });

  it('should contain a .', () => {
    myComponent.genMail();
    expect(myComponent.Mail).toContain('.');
  });

  it('should contain a Number (1)', () => {
    myComponent.genMail();
    expect(myComponent.Mail).toContain('1');
  });

  it('should contain the first letter of the name (M)', () => {
    myComponent.genMail();
    expect(myComponent.Mail).toContain('M');
  });

});
