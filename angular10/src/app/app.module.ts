import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PersoneComponent } from './persone/persone.component';
import { ShowPerComponent } from './persone/show-per/show-per.component';
import { AddEditPerComponent } from './persone/add-edit-per/add-edit-per.component';
import { SharedService } from './shared.service';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MailComponent } from './mail/mail.component';

@NgModule({
  declarations: [
    AppComponent,
    PersoneComponent,
    ShowPerComponent,
    AddEditPerComponent,
    MailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
