import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersoneComponent } from './persone/persone.component';
import { MailComponent } from './mail/mail.component';

const routes: Routes = [
{ path: 'persone', component: PersoneComponent },
{ path: 'mail', component: MailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
