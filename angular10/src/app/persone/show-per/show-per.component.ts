import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-per',
  templateUrl: './show-per.component.html',
  styleUrls: ['./show-per.component.css']
})
export class ShowPerComponent implements OnInit {


  constructor(private service: SharedService) { }

  PersoneList: any = [];
  modalTitle: string;
  ActivateAddEditPerComp: boolean;
  per: any;

  ngOnInit(): void {
    this.ActivateAddEditPerComp = false;
    this.refreshPersonList();
  }

  addClick(): void {
    this.per = {
      Id: 0,
      Name: '',
      Surname: '',
      Information: ''
    };

    this.modalTitle = 'Aggiungi Persona';
    this.ActivateAddEditPerComp = true;
  }

  editClick(item): void {
    this.per = item;
    this.modalTitle = 'Modifica Persona';
    this.ActivateAddEditPerComp = true;
  }


  closeClick(): void {
    this.ActivateAddEditPerComp = false;
    this.refreshPersonList();
  }

  refreshPersonList(): void {
    this.service.getPersoneList().subscribe(data => {
      this.PersoneList = data;
    });
  }
}
