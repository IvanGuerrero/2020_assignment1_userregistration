import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.css']
})
export class MailComponent implements OnInit {

  constructor() { }

  Name: string;
  Surname: string;
  Number: string;
  Domain: string;
  Mail: string;

  genMail(): void {
    if ( this.controlVar( this.Name) && this.controlVar( this.Surname) && this.controlVar( this.Number) && this.controlVar( this.Domain)){
      this.Mail = this.Name.charAt( 0) + '.' + this.Surname + this.Number + '@' + this.Domain;
    }
  }

  controlVar( v: string): boolean{
    let returnValue: boolean;
    returnValue = false;

    if ( v && v !== null  && v !== undefined){
      returnValue = true;
    }

    return returnValue;
  }

  ngOnInit(): void {
    this.Mail = '---';
  }

}
