import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SharedService } from './shared.service';

describe('SharedService', () => {
  let injector: TestBed;
  let service: SharedService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SharedService]
    });
    injector = getTestBed();
    service = TestBed.inject(SharedService);
    httpMock = injector.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('test Api getPersone', () => {
    it('should return an Observable<any[]>', () => {

      service.getPersoneList().subscribe(person => {
        expect(person.length).toBeGreaterThan(0);
      });

      const req = httpMock.expectOne(service.APIUrl + '/Persone/Get');
      expect(req.request.method).toBe('GET');
    });
  });
});
