import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-per',
  templateUrl: './add-edit-per.component.html',
  styleUrls: ['./add-edit-per.component.css']
})
export class AddEditPerComponent implements OnInit {

  constructor( private service: SharedService) { }

  @Input() per: any;
  Id: string;
  Name: string;
  Surname: string;
  Information: string;

  ngOnInit(): void {
    this.Id = this.per.Id;
    this.Name = this.per.Name;
    this.Surname = this.per.Surname;
    this.Information = this.per.Information;
  }

  addEditPersona(): void {
    const persona = 'Id=' + this.Id + '&Name=' + this.Name + '&Surname=' + this.Surname + '&Information=' + this.Information;

    this.service.addPersona(persona).subscribe( res => {
      alert(res.toString());
    });
  }
}
