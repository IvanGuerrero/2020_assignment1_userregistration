# Assignment 1 - Implementazione di una pipeline
prodotto da:
- Pievaioli Stefano, matricola 816592
- Presot Federico Davide, matricola 817290
- Bryan Ivan Zighui Guerrero, matricola 816335

# Stato attuale del progetto
La nostra applicazione in Angular è divisa in due parti: in una di esse, è possibile utilizzare un piccolo tool per generare un indirizzo mail; nella seconda è possibile caricare nome, cognome, e info aggiuntive su di una persona. Per questa applicazione sono stati implementati i seguenti passaggi della pipeline:
- Build: Durante la fase di build, vengono installati tutti i pacchetti all'interno della cache nel path angular10/node_modules/; Successivamente viene compilata l'app tramite ng build e i file generati vengono salvati in cache nel path angular10/dist/.  
- Verify: Durante la fase di verify, viene controllata la qualità del progetto Angular tramite le funzionalità di lint già presenti nella CLI di angular
- Unit-test: Durante la fase relativa agli unit test, vengono effettuati dei test utilizzando Karma-Jasmine tramite browser ChromeHeadless 
- Integration-test: Durante la fase di integration test, viene effettuato un test di integrazione per verificare il corretto funzionamento della nostra applicazione; viene di conseguenza effettuata una GET per verificare la corretta implementazione del nostro database
- Package: durante la fase di package, viene utilizzato Docker per generare un container nel quale è presente il nostro progetto; questa immagine viene poi salvata e taggata con il proprio valore in codifica SHA
- Release: Durante la fase di Release, inizialmente viene effettuato il login sulla piattaforma Docker; successivamente, il pachetto generato durante la fase di package viene taggata come latest release, e viene pushata nel docker container registry; infine, la stessa immagine viene pushata nel container registry di Heroku
- Deploy: Durante la fase di deploy, viene effettuato il deploy dell'immagine pushata nella fase di release. Il deploy viene effettuato sulla piattaforma Heroku utilizzando la sua API

l'applicazione è disponibile sul sito: https://prova-dot-net.herokuapp.com
(Essendo hostata su Heroku tramite un account gratuito, l'applicazione impiega 15 secondi per partire nel caso non sia stata effettuata una richiesta nell'ultima mezz'ora)
